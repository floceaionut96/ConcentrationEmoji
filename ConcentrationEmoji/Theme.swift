//
//  Theme.swift
//  ConcentrationEmoji
//
//  Created by Ionut Flocea on 21.02.2022.
//

import Foundation
import UIKit

struct Theme {
    
    var name: String
    var color: UIColor
    var emojiset: [String]
    
    init(name:String, color:UIColor, emojiset: [String]) {
        self.name = name
        self.color = color
        self.emojiset = emojiset
    }
    
}
