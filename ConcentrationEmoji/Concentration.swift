//
//  Concentration.swift
//  ConcentrationEmoji
//
//  Created by Ionut Flocea on 21.02.2022.
//

import Foundation

class Concentration{
    
    var cards = [Card]()
    
    var score = 0
    
    var flipCount = 0
    
    var indexOfOneAndOnlyFaceUpCard: Int?
    
    var gameThemes : [Theme] = [
        Theme(name: "Halloween", color: .orange, emojiset: ["🦇","😱","🙀","👿", "🎃", "👻"]),
        Theme(name: "Animals", color: .green, emojiset: ["🐈", "🐮", "🐖", "🐒", "🐍", "🐘"]),
        Theme(name: "Music", color: .yellow, emojiset: ["🎶", "🎹", "🎻", "🎸", "🥁", "🎷"]),
        Theme(name: "Vehicles", color: .red, emojiset: ["🚗", "✈️", "🚔", "🏎", "🏍", "🚁"]),
        Theme(name: "Tools", color: .gray, emojiset: ["🔧", "⛏", "🔩", "⚙️", "🪚", "🪓"]),
        Theme(name: "Food", color: .systemPink, emojiset: ["🍔", "🍗", "🍿", "🍩", "🍫", "🥞"])
    
    ]
    
    func chooseCard(at index: Int){
        flipCount += 1
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                //check if cards match
                if cards[matchIndex].identifier == cards[index].identifier
                {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    score += 2
                } else{
                    score -= 1
                }
               
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = nil
            } else {
                //either no cards or 2 cards are face up
                for flipDownIndex in cards.indices{
                    cards[flipDownIndex].isFaceUp = false
                }
                
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    init(numberOfPairsOfCards: Int){
        for _ in 0..<numberOfPairsOfCards{
        let card = Card()
        cards += [card, card]
        }
        cards.shuffle()
        score = 0
        flipCount = 0
    }
    
    func getTheme() -> Theme {
        return gameThemes.randomElement()!
    }
    
    
}
