//
//  ViewController.swift
//  ConcentrationEmoji
//
//  Created by Ionut Flocea on 21.02.2022.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards:cardButtons.count/2 )
    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBOutlet weak var themeName: UILabel!
    
    @IBOutlet var cardButtons: [UIButton]!
    
    @IBOutlet weak var newGameBttOutlet: UIButton!
    
    @IBOutlet weak var scoreOutlet: UILabel!
    
    lazy var theme = game.getTheme()
    
    var emoji = [Int:String]()
    
    
    @IBAction func touchCard(_ sender: UIButton) {
        
        if let cardNumber = cardButtons.firstIndex(of: sender){
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
        
        scoreOutlet.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flipCount)"
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUiColors()
    }
    
    
    @IBAction func newGameBttPressed(_ sender: UIButton) {
         game = Concentration(numberOfPairsOfCards:cardButtons.count/2)
        theme = game.getTheme()
        setUiColors()
        
    }
    
    func updateViewFromModel(){
        for index in cardButtons.indices{
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp{
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = .white
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? .clear : theme.color
            }
        }
    }
    
    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, theme.emojiset.count > 0 {
                
            let randomIndex = Int(arc4random_uniform(UInt32(theme.emojiset.count)))
            emoji[card.identifier] = theme.emojiset.remove(at: randomIndex)
                
            }
        return emoji[card.identifier] ?? "?"
    }
    
    func flipCard(withEmoji emoji: String, on button: UIButton){
        if button.currentTitle == emoji {
            button.setTitle("", for: UIControl.State.normal)
            button.backgroundColor = theme.color
        } else {
            button.setTitle(emoji, for: UIControl.State.normal)
            button.backgroundColor = .white
        }
    }
    
    func setUiColors(){
        for button in cardButtons{
            button.backgroundColor = theme.color
            button.setTitle("", for: UIControl.State.normal)
        }
        flipCountLabel.textColor = theme.color
        themeName.text = theme.name
        themeName.textColor = theme.color
        scoreOutlet.textColor = theme.color
        scoreOutlet.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flipCount)"
    }


}

