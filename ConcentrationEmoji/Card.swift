//
//  Card.swift
//  ConcentrationEmoji
//
//  Created by Ionut Flocea on 21.02.2022.
//

import Foundation

struct Card{
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    static var identifierFactory = 0
    
    static func getUniqueIdenttifier() -> Int {
        Card.identifierFactory += 1
        return Card.identifierFactory
    }
    
    init(){
        self.identifier = Card.getUniqueIdenttifier()
    }
}
